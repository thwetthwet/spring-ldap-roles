package com.thwet.spring_ldap_roles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class SpringLdapRolesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLdapRolesApplication.class, args);
	}

}
